require.config({
  baseUrl: '../js/',
  paths: {
    'jquery': '../js/libs/jquery/dist/jquery.min',
    'underscore': '../js/libs/underscore/underscore',
    'backbone': '../js/libs/backbone/backbone',
    'marionette': '../js/libs/marionette/lib/backbone.marionette.min',
    'wreqr': '../js/libs/backbone.wreqr/lib/backbone.wreqr.min',
    'babysitter': '../js/libs/backbone.babysitter/lib/backbone.babysitter.min',
    shim: {
      'underscore': {
        exports: '_'
      },
      'backbone': {
        deps: ['underscore', 'jquery'],
        exports: 'Backbone'
      },
      'marionette': {
        deps: ['wreqr', 'babysitter']
      }
    }
  }
});

require(['models/pupilsClass'], function (PupilsClass) {

  var model = new PupilsClass;
  model.set('name', '11');

  module('Business logic tests');
  test('Should calculate marks\'s average value', function () {
    model.set('pupils', 12);
    model.set('marks', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
    model.calcAverage();
    equal(model.get('average'), 6.5, 'Average value is calculated correctly with [1,2,3,4,5,6,7,8,9,10,11,12]');

    model.set('pupils', 5);
    model.set('marks', [10, 10, 10, 10, 10]);
    model.calcAverage();
    equal(model.get('average'), 10, 'Average value is calculated correctly with [10,10,10,10,10]');

    model.set('pupils', 8);
    model.set('marks', [10, 11, 8, 6, 5, 2, 12, 1]);
    model.calcAverage();
    equal(model.get('average'), 6.9, 'Average value is calculated correctly [10,11,8,6,5,2,12,1]');
  });

  test('Should marks on values', function () {
    model.set('pupils', 12);
    model.set('marks', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);

    model.sortMarksOnValues();

    for (var i = 1; i <= 12; i++) {
      equal(model.get('numOf' + i), 1, 'Marks is sorted correctly');
    }
  });

  test('Should sort marks on levels', function () {
    model.set('pupils', 12);
    model.set('marks', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);

    model.sortMarksOnLevels();

    equal(model.get('highLevelPercent'), 25, 'High level is calculated correctly');
    equal(model.get('middleLevelPercent'), 25, 'Middle level is calculated correctly');
    equal(model.get('beginnerLevelPercent'), 25, 'Beginner level is calculated correctly');
    equal(model.get('sufficientLevelPercent'), 25, 'Sufficient level is calculated correctly');
  });

  test('Should calculate quality percent', function (){
    model.set('sufficientLevelPercent','25.1');
    model.set('highLevelPercent','22.8');
    model.calcQuality();
    equal(model.get('quality'),47.9,'Quality percent is calculated correctly');

    model.set('sufficientLevelPercent','25');
    model.set('highLevelPercent','5.0');
    model.calcQuality();
    equal(model.get('quality'),30,'Quality percent is calculated correctly');
  });

  test('Should calculate success percent', function (){
    model.set('beginnerLevelPercent','1');
    model.calcSuccess();
    equal(model.get('successPercent'),99,'Success percent is calculated correctly');

    model.set('beginnerLevelPercent','25.8');
    model.calcSuccess();
    equal(model.get('successPercent'),74.2,'Success percent is calculated correctly');

    model.set('beginnerLevelPercent','0.5');
    model.calcSuccess();
    equal(model.get('successPercent'),99.5,'Success percent is calculated correctly');
  });
});
