require.config({
  baseUrl: 'js/',
  paths : {
    'jquery': 'libs/jquery/dist/jquery.min',
    'underscore': 'libs/underscore/underscore',
    'backbone': 'libs/backbone/backbone',
    'marionette' : 'libs/marionette/lib/backbone.marionette.min',
    'wreqr': 'libs/backbone.wreqr/lib/backbone.wreqr.min',
    'babysitter' : 'libs/backbone.babysitter/lib/backbone.babysitter.min',
    shim: {
      'underscore': {
        exports: '_'
      },
      'backbone': {
        deps: ['underscore', 'jquery'],
        exports: 'Backbone'
      },
      'marionette' : {
        deps: ['wreqr', 'babysitter']
      }
    }
  }
});

require(['controllers/app-controller'], function () {});
