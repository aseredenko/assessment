define('collections/classesCollection', ['backbone', 'underscore'], function (Backbone) {
  var ClassesCollection = Backbone.Collection.extend({});
  var classesCollection = new ClassesCollection;

  return classesCollection;
});
