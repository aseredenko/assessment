define('controllers/dispatcher', ['backbone'],  function(Backbone) {
    return _.clone(Backbone.Events);
  }
);