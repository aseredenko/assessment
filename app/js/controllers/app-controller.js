define('controllers/app-controller',
  ['backbone',
    'jquery',
    'marionette',
    'models/pupilsClass',
    'views/collectionView',
    'views/appView',
    'views/stepsView',
    'controllers/dispatcher'
    ],
  function (Backbone, $, Marionette, PupilsClass, CollectionView, AppView, StepsView, dispatcher) {
    /**
     * Add new model to the collection
     * @param {Backbone.Collection} collection - Collection for adding new model
     */
    dispatcher.on('addNewClass', function(collection){
      var pupilsClass = new PupilsClass;
      collection.add(pupilsClass);

      $('#final-table').prop('class',pupilsClass.cid);
    });

    /**
     * Set name and number of pupils values to the model
     * @param {String} className - The name of the class
     * @param {Number} numOfPupils - Number of the pupils in the class
     * @param {Backbone.Model} model - Current model
     */
    dispatcher.on('setNameForClass', function(className, numOfPupils, model) {
      model.set('name', className);
      model.set('pupils', numOfPupils);
    });

    /**
     * Remove model from the collection
     * @param {Backbone.Collection} collection - current collection
     * @param {String} modelCid - cid value of the current model
     */
    dispatcher.on('removeClass', function(collection, modelCid){
      collection.get(modelCid).destroy();
    });

    /**
     * Set marks values to the model
     * Set marks values to the model
     * @param {Backbone.Model} model - Current model
     * @param {Array} marksArray - array with marks
     */
    dispatcher.on('addMarks', function(model, marksArray){
      model.set('marks', marksArray);
    });

    /**
     * Calculate and set average value to the model
     * @param {Backbone.Model} model - Current model
     */
    dispatcher.on('calcAverage', function(model){
      model.calcAverage();
    });

    /**
     * Sort marks on values and set it to the model
     * @param {Backbone.Model} model - Current model
     */
    dispatcher.on('sortMarksOnValues', function(model){
      model.sortMarksOnValues();
    });

    /**
     * Sort marks on levels
     * @param {Backbone.Model} model - Current model
     */
    dispatcher.on('sortMarksOnLevels', function(model){
      model.sortMarksOnLevels();
    });

    /**
     * Calculate and set quality value to the current model
     * @param {Backbone.Model} model - Current model
     */
    dispatcher.on('calcQuality', function(model){
      model.calcQuality();
    });

    /**
     * Calculate and set success percent to the current model
     * @param {Backbone.Model} model - Current model
     */
    dispatcher.on('calcSuccess', function(model){
     model.calcSuccess();
    });


    var collectionView = new CollectionView;
    var appView = new AppView;
    var stepsView = new StepsView;

  });