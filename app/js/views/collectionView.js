define('views/collectionView',
  ['backbone',
    'jquery',
    'underscore',
    'marionette',
    'collections/classesCollection'
    ],
  function (Backbone, $, _, Marionette, classesCollection){
    /**
     * Create a new collection view
     * @class CollectionView
     * @author Alexander Seredenko
     */
    var CollectionView = Backbone.Marionette.CollectionView.extend({
      collection: classesCollection,
      collectionEvents: {
        'add': 'showClassInput',
        'remove': 'removeClassInput'
      },

      /**
       * Show inputs for new class on the page
       * @method showClassInput
       * @param {Backbone.model} currentClass - Current class model
       */
      showClassInput: function (currentClass) {
        var classInputTmp = _.template($('#class-input-tmp').html());
        $('#names-of-classes').append(classInputTmp({'classCid': currentClass.cid}));
      },

      /**
       * Remove inputs for class on the page
       * @method showClassInput
       * @param {Backbone.model} classModel - Current class model
       */
      removeClassInput: function (classModel) {
        var classCid = classModel.cid;
        $('#' + classCid).remove();
      }
    });

    return CollectionView;
});