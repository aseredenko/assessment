define('views/appView',
  ['backbone',
    'jquery',
    'collections/classesCollection',
    'controllers/dispatcher'],
  function (Backbone, $, classesCollection, dispatcher) {
    /**
     * Create a new App View
     * @class AppView
     * @author Alexander Seredenko
     */
    var AppView = Backbone.View.extend({
      el: 'body',
      collection : classesCollection,
      initialize: function (){
        dispatcher.trigger('addNewClass', this.collection);
      },
      events: {
        'click #add-new-class': 'addNewClass',
        'click .submit-name-of-class' : 'setNameForClass',
        'click .remove-class': 'removeClass',
        'click #calculate-marks' : 'getMarks'
      },
      /**
       * Call to **dispatcher** for adding new model to the collection
       * @method addNewClass
       */
      addNewClass: function () {
        dispatcher.trigger('addNewClass', this.collection);
      },
      /**
       * Get  current **cid** from the page and call the **dispatcher** for removing model from the collection
       * @method removeClass
       * @param {Object} e - Events object
       */
      removeClass: function (e){
        var modelCid = $(e.currentTarget).parent('div').prop('id');
        dispatcher.trigger('removeClass', this.collection, modelCid);
      },
      /**
       * Get marks values from the forms inputs and call the **dispatcher** for calculation with them
       * @method calculateMarks
       */
      getMarks : function() {

        var isValid = true;

        $('.marks-values').find('input[type="text"]').each( function (){
          if ( $(this).val() != '' && parseInt($(this).val()) <= 12 && parseInt($(this).val()) >= 1) {
            $(this).removeClass('warning');
          } else {
            $(this).addClass('warning');
            isValid = false;
          }
        });

        if ( isValid === false ){
          return 0;
        }

        var currentCollection = this.collection;
        var currentView = this;
        var commonMarksArray = [];

        $('#second-step').find('.marks').each( function (){

          var currentForm = $(this).find('.marks-values');
          var marksArray = [];

          currentForm.find('input[type="text"]').each( function (){
            var currentMark = $(this).val();
            commonMarksArray.push(currentMark);
            marksArray.push(currentMark);
          });

          var containerClasses = $(this).prop('class').split(' ');
          var modelCid = containerClasses[1];

          var model = currentCollection.get(modelCid);


          currentView.marksCalculation(model,marksArray);
        });

        var commonModel = currentCollection.get($('#third-step').attr('data-cid'));
        currentView.marksCalculation(commonModel,commonMarksArray);
        console.log(commonModel);
      },
      marksCalculation : function (model, marksArray) {
        /**
         * Call the **dispatcher** for adding marks to the current model
         */
        dispatcher.trigger('addMarks', model, marksArray);

        /**
         * Call the **dispatcher** for calculating average value
         */
        dispatcher.trigger('calcAverage', model, marksArray);

        /**
         * Call the **dispatcher** for sorting marks on values
         */
        dispatcher.trigger('sortMarksOnValues', model, marksArray);

        /**
         * Call the **dispatcher** for sorting marks on levels
         */
        dispatcher.trigger('sortMarksOnLevels', model, marksArray);

        /**
         * Call the **dispatcher** for calculating quality value
         */
        dispatcher.trigger('calcQuality', model);

        /**
         * Call the *dispatcher* for calculating success percent
         */
        dispatcher.trigger('calcSuccess', model);
      }

    });

    return AppView;
});
