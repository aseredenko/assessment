define('views/stepsView',
  ['backbone',
    'jquery',
    'marionette',
    'collections/classesCollection',
    'controllers/dispatcher',
    'models/pupilsClass'],
  function (Backbone, $, Marionette, classesCollection, dispatcher, PupilsClass) {
    /**
     * Add new Steps view
     * @class StepsView
     * @author Alexander Seredenko
     */
    var StepsView = Backbone.Marionette.View.extend({
      el : 'body',
      collection : classesCollection,
      events: {
        'click #classes-ready': 'goToSecondStep',
        'click #calculate-marks' : 'showFinalTable',
        'click #go-back' : 'goBackForEdit'
      },
      /**
       * Validate inputs
       * Add new classes models
       * Show second step using model values added in first step
       * @method goToSecondStep
       */
      goToSecondStep: function () {
        /**
         * Check and validate values in the inputs
         */
        var isValid = true;

        $('.class-input').find('input[type="text"]').each( function (){
          if ( $(this).val() != '' ) {
            $(this).removeClass('warning');
          } else {
            $(this).addClass('warning');
            isValid = false;
          }
        });

        if ( isValid === false ){
          return 0;
        }

        var currentCollection = this.collection;
        /**
         * Set data for models
         */
        var sumOfPupils = 0;
        $('.class-input').each( function (){
          var currentElement = $(this);
          var className = currentElement.find('.name-value').val();
          var numOfPupils = currentElement.find('.num-of-pupils').val();
          var modelCid = currentElement.prop('id');
          var model = currentCollection.get(modelCid);
          sumOfPupils += parseInt(numOfPupils);

          dispatcher.trigger('setNameForClass', className, numOfPupils, model);
        });

        var pupilsClass = new PupilsClass;
        this.collection.add(pupilsClass);
        dispatcher.trigger('setNameForClass', "Всього", sumOfPupils, pupilsClass);
        $('#third-step').attr('data-cid',pupilsClass.cid);

        $('#first-step').hide();
        $('#second-step').show();

        var collectionLength = this.collection.length;
        var classesArray = this.collection.models;

        for (var i = 0; i < collectionLength-1; i++) {
          var currentCid = classesArray[i].cid;
          var currentModel = this.collection.get(currentCid);
          var currentNumOfPupils = currentModel.get('pupils');
          var currentClassName = currentModel.get('name');

          /**
           * Generate inputs for marks
           * @function inputs
           * @param numOfPupils
           * @returns {String} string - Inputs for marks
           */
          var inputs = function (numOfPupils) {
            var string = '';
            for (var j = 0; j < numOfPupils; j++) {
              string += '<input type="text" />';
            }
            return string;
          };

          var marksInputsTmp = _.template($('#class-marks-tmp').html());
          $('#second-step').append(marksInputsTmp({
            'classCid': currentCid,
            'className': currentClassName,
            inputsForMarks: inputs(currentNumOfPupils)
          }));
        }
        var buttonTmp = _.template('<button id="calculate-marks" class="btn">Порахувати результати</button>');
        $('#second-step').append(buttonTmp);
      },

      /**
       * Validate inputs
       * Show final table wit calculation results and models values
       * @method showFinalTable
       */
      showFinalTable: function () {
        /**
         * Validate inputs
         */
        var isValid = true;

        $('.marks-values').find('input[type="text"]').each( function (){
          if ( $(this).val() != '' && parseInt($(this).val()) <= 12 && parseInt($(this).val()) >= 1) {
            $(this).removeClass('warning');
          } else {
            $(this).addClass('warning');
            isValid = false;
          }
        });

        if ( isValid === false ){
          return 0;
        }

        var collectionLength = this.collection.length;
        var classesArray = this.collection.models;

        for (var i = 0; i < collectionLength; i++) {
          var currentCid = classesArray[i].cid;
          var currentModel = this.collection.get(currentCid);

          var classObject = {
            'name': currentModel.get('name'),
            'pupils': currentModel.get('pupils'),
            'numOf12': currentModel.get('numOf12'),
            'numOf11': currentModel.get('numOf11'),
            'numOf10': currentModel.get('numOf10'),
            'numOf9': currentModel.get('numOf9'),
            'numOf8': currentModel.get('numOf8'),
            'numOf7': currentModel.get('numOf7'),
            'numOf6': currentModel.get('numOf6'),
            'numOf5': currentModel.get('numOf5'),
            'numOf4': currentModel.get('numOf4'),
            'numOf3': currentModel.get('numOf3'),
            'numOf2': currentModel.get('numOf2'),
            'numOf1': currentModel.get('numOf1'),
            'highLevelPercent': currentModel.get('highLevelPercent'),
            'sufficientLevelPercent': currentModel.get('sufficientLevelPercent'),
            'middleLevelPercent': currentModel.get('middleLevelPercent'),
            'beginnerLevelPercent': currentModel.get('beginnerLevelPercent'),
            'average': currentModel.get('average'),
            'successPercent': currentModel.get('successPercent'),
            'quality': currentModel.get('quality')
          };
          var classRowTmp = _.template($('#class-row-tmp').html());
          $('#final-table').append(classRowTmp(classObject));
        }

        $('#second-step').hide();
        $('#third-step').show();

      },
      goBackForEdit: function () {
        $('#third-step table>tr').remove();
        $('#third-step').hide();
        $('#second-step').show();
      }
    });

    return StepsView;
  });

/* dispatcher.trigger('addNewClass', currentCollection);
 var commonModelCid = $('#final-table').prop('class');
 var commonModel = currentCollection.get(commonModelCid);
 currentView.marksCalculation(commonModel,commonMarksArray);
 console.log(currentCollection.models);*/

/* dispatcher.trigger('setNameForClass', "Всього", numOfPupils, commonModel);*/