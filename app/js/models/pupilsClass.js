define('models/pupilsClass', ['backbone'], function (Backbone){
  /**
   * /**
   * Create a new PupilsClass model
   * @class AppView
   * @author Alexander Seredenko
   */
  var PupilsClass = Backbone.Model.extend({
    /**
     * Calculate and set average value to the model
     * @method calcAverage
     */
    calcAverage: function (){
      var sum = 0;
      var marksArray = this.get('marks');

      marksArray.forEach( function (currentValue) {
        sum += parseInt(currentValue);
      });

      var average = (sum/marksArray.length).toFixed(1);

      this.set('average', average);
    },
    /**
     * Sort marks on values and set it to the model
     * @method sortMarksOnValues
     */
    sortMarksOnValues: function () {
      var marksArray = this.get('marks');

      for(var i=1; i<=12;i++){
        var currentValueArray = marksArray.filter(function (element) {
          return element==i;
        });

        this.set('numOf' + i,currentValueArray.length);
      }
    },
    /**
     * Sort marks on levels
     * @method sortMarksOnLevels
     */
    sortMarksOnLevels: function () {
      var marksArray = this.get('marks');
      var numOfPupils = this.get('pupils');

      var highLevelArray =  marksArray.filter(function (element){
        return element>=10;
      });
      var highLevel = highLevelArray.length;
      var highLevelPercent = (highLevel/numOfPupils * 100).toFixed(1);
      this.set('highLevelPercent',highLevelPercent);

      var middleLevelArray = marksArray.filter(function (element){
        return element<=6 && element>=4;
      });
      var middleLevel = middleLevelArray.length;
      var middleLevelPercent = (middleLevel/numOfPupils * 100).toFixed(1);
      this.set('middleLevelPercent',middleLevelPercent);

      var beginnerLevelArray = marksArray.filter(function (element) {
        return element<=3 && element>=1;
      });
      var beginnerLevel = beginnerLevelArray.length;
      var beginnerLevelPercent = (beginnerLevel/numOfPupils * 100).toFixed(1);
      this.set('beginnerLevelPercent',beginnerLevelPercent);

      var sufficientLevelPercent = (1000 - beginnerLevelPercent*10 - middleLevelPercent*10 - highLevelPercent*10)/10;
      this.set('sufficientLevelPercent', sufficientLevelPercent);
    },
    /**
     * Calculate and set quality value to the current model
     * @method calcQuality
     */
    calcQuality: function () {
      var sufficientLevelPercent = this.get('sufficientLevelPercent');
      var highLevelPercent = this.get('highLevelPercent');
      var qualityPercent = (parseFloat(sufficientLevelPercent)*10 + parseFloat(highLevelPercent)*10)/10;

      this.set('quality', qualityPercent);
    },
    /**
     * Calculate and set success percent to the current model
     * @method calcSuccess
     */
    calcSuccess: function () {
      var beginnerLevelPercent = this.get('beginnerLevelPercent');
      var successPercent = 100 - beginnerLevelPercent;

      this.set('successPercent', successPercent);
    }
  });

  return PupilsClass;
});